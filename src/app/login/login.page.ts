import { Component, OnInit } from '@angular/core';
import { AppAvailability } from '@ionic-native/app-availability/ngx';
import { Platform } from '@ionic/angular';
import { LinkedIn, OauthCordova } from 'ionic-cordova-oauth';



@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  isLoggedIn: boolean = false;
  private oauth: OauthCordova = new OauthCordova();
  
  private linkedinProv: LinkedIn = new LinkedIn({
    clientId : 'm.sergiovp@gmail.com',
    appScope : ['email-address'],
    redirectUri : 'http://localhost:8100'
  });


  constructor( private plt:Platform, private appAvlvty: AppAvailability, private linkedin:LinkedIn) { }

  ngOnInit() {

    //verificamos tipo de plataforma ##funciona
    let app :any;
    let plat: any;
    if (this.plt.is('ios')) {
        plat = 'ios'
        app = 'linkedin://';
        console.log(app);
        console.log(plat);


    } else if (this.plt.is('android')) {
        plat = 'android'
        app = 'com.linkedin.android';
        console.log(app);
        console.log(plat);
        
    }

    //verificamos existencia de aplicacion ##Funciona
    this.appAvlvty.check(app)
    .then(
    (yes: boolean) => console.log(app + ' is available'),
    (no: boolean) => console.log(app + ' is NOT available')
    );
  }

  //Corregir
  login(){
    this.plt.ready().then(()=>{
      this.oauth.logInVia(this.linkedinProv)
      .then(success =>{
        console.log("Logrado :) :",success);     
      }, error =>{
        console.log("Error:", error);
        
      })
    })
  }


}

