import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
  /**
   * Aqui tenemos un arreglo de elementos 
   * los cuales nos permite poder relizar el enrutamiento de elementos en nuesta aplicacion
   * 
   */
const routes: Routes = [

  { path: '', redirectTo: 'login', pathMatch: 'full' }, /**Agregamos como pagina root nuestro login */
  { path: 'home', loadChildren: './home/home.module#HomePageModule'},
  { path: 'list', loadChildren: './list/list.module#ListPageModule'},
  { path: 'login', loadChildren: './login/login.module#LoginPageModule'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

